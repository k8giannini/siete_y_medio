# My project's README

This project is a single player game of siete y medio.

**Classes**

* Player: keeps track of a hand of cards, money, bets, and points

* Hand: keeps track of a vector of Cards

* Card: keeps track of suit and rank, default constructor randomly generates a card


**Non-Member functions**

* round_of_siete_y_medio: takes in two Players the first being the user and the second being the dealer
and simulates a round of siete y medio

* card_display: takes in a Card and displays the spanish rank and suit followed by the english

* game_log: takes in two players a string and an int, the players being the user and the dealer and the string 
being a file and the int being the game count and sends stats to the file specified

* game_clean_up: takes in two Players, the user and the dealer and resets their hand and bet