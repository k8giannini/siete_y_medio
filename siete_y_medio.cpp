

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "cards.h"
using namespace std;

// Global constants
const int MONEY = 100;
const int DEALER_MONEY = 900;
const double WINNING_SCORE = 7.5;
const int MAX_MONEY = 1000;
const int MIN_MONEY = 0;
const double DEALER_MIN = 5.5;

// non member functions
void round_of_siete_y_medio(Player& person, Player& dealer);
void card_display(const Card& c);
void game_log(const Player& person, const Player& dealer, string file, int game_count);
void game_clean_up(Player& person, Player& dealer);

// displays spanish rank and suit followed by english version
void card_display(const Card& c){
    cout << c.get_spanish_rank() << " of " << c.get_spanish_suit() <<"\t";
    cout << "(" << c.get_english_rank() << " of " << c.get_english_suit() << ")" << endl << endl;
}

// creates game log
void game_log(const Player& person, const Player& dealer, string file, int game_count){
    ofstream out;
    out.open(file);
    
    //will not write anything if file is not correct
    if(out.is_open()){
        
        out << "-----------------------------------------------" << endl << endl;
        out << "Game number: " << game_count << "\tMoney left: $" << person.get_money() << endl;
        out << "Bet: " << person.get_bet() << endl << endl;
        
        //player outcome
        out << "Your cards: "<< endl;
        person.display(out);
        out << "Your total is: " << person.get_points() << "." << endl << endl;
        
        //dealer outcome
        out << "Dealer's cards: " << endl;
        dealer.display(out);
        out << "Dealer's total is: " << dealer.get_points() << "." << endl;
    }
}

// plays a round og siete y medio
void round_of_siete_y_medio(Player& person, Player& dealer, int game_count, string log_file){
    
    //initializing the bet
    unsigned int bet = 0;
    
    cout << "You have $"<<person.get_money()<<". Enter bet: ";
    cin >> bet;
    
    // check for valid bet
    while (bet > person.get_money()){
        cout << endl << "Sorry invalid bet. Enter valid bet: ";
        cin >> bet;
    }
    
    //setting the bet
    person.set_bet(bet);
    
    //adding card to hand
    person.add_Card(person.draw());
    cout << "Your cards: " << endl;
    person.display(cout);
    
    //determining if player wants another card
    cout << "Your total is "<< person.get_points() << ". Do you want another card (y/n)? ";
    char play;
    cin >> play;
    
    //while they want to play
    while(play == 'y'){
        //new card to add to hand
        Card c;
        cout << endl <<"New card: " << endl;
        card_display(c);
        
        person.add_Card(c);
        cout << "Your cards: " << endl;
        person.display(cout);
        
        //asking if they want to continue
        cout << "Your total is "<< person.get_points() << ". Do you want another card (y/n)? ";
        cin >> play;
    }
    
    
    //dealers play
    dealer.add_Card(dealer.draw());
    cout << endl <<"Dealer's cards: " << endl;
    dealer.display(cout);
    cout << "The dealer's total is "<< dealer.get_points() << endl;
    
    //dealer continues to draw until they have more than 5.5 points
    while(dealer.get_points() < DEALER_MIN){
        
        // new card
        Card d;
        cout << endl << "New card: " << endl;
        card_display(d);
        
        // new stats
        dealer.add_Card(d);
        cout << "Dealer's cards: " << endl;
        dealer.display(cout);
        cout << "The dealer's total is "<< dealer.get_points() << endl;
    }
    
    bool win;
    bool draw;
    int money;
    
    if(person.get_points()>WINNING_SCORE){
        cout << "Too bad. You lose " << person.get_bet() << "." << endl;
        win = false;
        draw = false;
    }
    else if(dealer.get_points()>WINNING_SCORE){
        cout << "You win " << person.get_bet() << "." << endl;
        win = true;
        draw = false;
    }
    else if(dealer.get_points()>person.get_points()){
        cout << "Too bad. You lose " << person.get_bet() << "." << endl;
        win = false;
        draw = false;
    }
    else if(dealer.get_points() == person.get_points()){
        cout << "It's a DRAW!!" << endl;
        win = false;
        draw = true;
    }
    else{
        cout << "You win " << person.get_bet() << "." << endl;
        win = true;
        draw = false;
    }
    
    game_log(person, dealer, log_file, game_count);
    
    
    if(win){
        money = person.get_bet() + person.get_money();
        person.set_money(money);
    }
    if(!win && !draw){
        money = person.get_money() - person.get_bet();
        person.set_money(money);
    }
    
    
}

void game_clean_up(Player& person, Player& dealer){
    person.clean();
    dealer.clean();
}


// main routine
int main(){
    Player person(MONEY);
    Player dealer(DEALER_MONEY);
    int game_count = 1;
    
    //now will complile not in xcode, and not specific to anyones system
    string log_file = "gamelog.txt";
    
    while(person.get_money() > MIN_MONEY && person.get_money() < MAX_MONEY){
        round_of_siete_y_medio(person, dealer, game_count, log_file);
        game_count++;
        game_clean_up(person, dealer);
    }
    
    if(person.get_money() == MIN_MONEY){
        cout << "You have $0. GAME OVER!" << endl << "Come back when you have more money.";
    }
    if(person.get_money() >= MAX_MONEY){
        cout << "Congratulations. You beat the casino!" << endl;
    }
    
    cout << endl << "Bye!";
    
    return 0;
}
